#!/usr/bin/perl -w

use CGI qw/:all/;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;
use CGI::Cookie;
use File::Path qw(make_path remove_tree);

warningsToBrowser(1);
%cookies = fetch CGI::Cookie;
my $username = param('username') || '';
my $password = param('password') || '';
# Set current user and password
my $cookie_user = '';
my $cookie_pass = '';
$cookie_user = $cookies{'cookie_user'}->value if $cookies{'cookie_user'};
$cookie_pass = $cookies{'cookie_pass'}->value if $cookies{'cookie_pass'};
my $crr_pswd = param('crr_pswd') || '';
# Set page state
my $page_state = '';
$page_state = $cookies{'state'}->value if $cookies{'state'};
# Set current user page
$page_user = '';
$page_user = $cookies{'page_user'}->value if $cookies{'page_user'};
# Set mate state when I'm visiting mate.
my $mate = param ('mate')||'';
my $login = param('Login') || '';
my $logout = param('Logout') || '';
# Set search mate regex
my $search = param('search') || '';
# Set seach post regex
my $search_p = param('search_p') || '';
# Set post texts
my $post_text = param('post_text') || '';
my $write_post = param('Write') || '';
# Set comment num for commenting on post
my $com_num = param('com_num') || '';
my $com_text = param('com_text') || '';
my $write_com = param('write_com') || '';
# set delete post number
my $del_num = param('del_num') || '';
# Set delete comment number
my $del_com_num = param('del_com_num') || '';
# Set delete user Zid
my $del_user = param('del_user') || '';
# Set add user zid
my $add_user = param('add_user') || '';
# Set rand number for validation
my $rand_num = '';
$rand_num = $cookies{'rand_num'}->value if $cookies{'rand_num'};
# Set details for registration
my $reg_user = param('reg_user') || '';
my $reg_pass = param('reg_pass') || '';
my $reg_name = param('reg_name') || '';
my $reg_email = param('reg_email') || '';
my $reg_birth = param('reg_birth') || '';
my $reg_pro = param('reg_pro') || '';
my $reg_sbrb = param('reg_sbrb') || '';
my $reg_course = param('reg_course') || '';
my $reg_num = param('reg_num') || 'hello';
my @reg_error;
# Set variable for adding profile text
my $profile_text = param('profile_text') || '';
my $profile_add = param('profile_add') || '';
# Set variable for uploading image
my $load_image = param('load_image') || '';
# Set variable for privacy setting
my $privacy = param('privacy') || '';
# Set warning states
my $error = 0;
my $email_error;
my $chng_detail;
sub main() {
	# print start of HTML ASAP to assist debugging if there is an error in the script
	$users_dir = "dataset-medium";
	# define some global variables
	$debug = 0;
	# Now tell CGI::Carp to embed any warning in HTML
	warningsToBrowser(1);
    # Change user details in setting if Change button pressed
    if (defined param('change_pass') || defined param('change_name') || defined param('change_email')||defined param('change_birth') || defined param('change_pro') || defined param('change_sbrb') ||defined param('change_course')) {
        change_detail();
    }
    # Set privacy
    if (defined param('privacy')){
        set_privacy(); 
        my $curr_privacy = check_privacy($cookie_user);
        if ($curr_privacy == 4){
            my $path = "$users_dir/$cookie_user";
            remove_tree($path);
            $page_state = '';
            $cookie_user = '';
            $cookie_pass = '';

        }
    }
    # Move to find password page
    if (defined param('Lost Password')){
        $page_state = 'find_password_page';
    }
    # Move to setting page
    if (defined param('Setting')){
        $page_state = "setting_page";
    }
    if (defined param('Back')){
        $page_state = 'user_page';
    }
    # Write profile text
    if (defined param('write_profile')){
        write_profile_text();
        $profile_add = 0;
    }
    # Check sign-up
    if (defined param('Sign Up')){
        if (check_reg_detail()){
            create_user();   
            $page_state = "register_complete";

        }
    }
    # Delete profile image 
    if (defined param('load_image')){
        if ($load_image == 2){
            delete_profile_img();
        }
        if ($load_image == 3){
            delete_back_img();
        }
    }
    # Send temporary password
    if (defined param('Find')){
        $rand_num = 1 + int rand(100000);
        my $message = "Use this temporary password: $rand_num\n Please Change Password in setting\n";
        my $path = "$users_dir/$reg_user/user.txt";
        my @user_detail;
        my $email;
        open DETAIL, '<', $path;
        foreach my $line (<DETAIL>){
            if ($line =~ /^password=(.*)$/){
                $line = "password=$rand_num\n";
            }
            if ($line =~/^email=(.*)$/){
                $email = $1;
                chomp $email;
            }
            push @user_detail, $line;
        }
        unlink $path;
        open FILE, '>'.$path;
        foreach my $detail (@user_detail){
            print FILE $detail;
        }
        close FILE;
        open (MAIL, "|/usr/sbin/sendmail -t");
        print MAIL "To: $email\n";
        print MAIL "From: Matelook\n";
        print MAIL "Subject: [Matelook] Lost Password\n";
        print MAIL $message;
        close(MAIL);
        
    }
    # Send rand number for email validation
    if (defined param('Validate')){
        $rand_num = 1 + int rand(100000);
        my $message = "Fill in this number to validate: $rand_num\n";
        open (MAIL, "|/usr/sbin/sendmail -t");
        print MAIL "To: $reg_email\n";
        print MAIL "From: Matelook\n";
        print MAIL "Subject: [Matelook] Validate Email\n";
        print MAIL $message;
        close(MAIL);

    }
    # Move to register page
    if (defined param('Register')){
        $page_state = 'register_page';
    }
    # Exit to login page
    if (defined param('Exit')){
        $page_state = '';
    }
    # Add user
    if (defined param('add_user')){
        add_mate($add_user);
    }
    # Delete user
    if (defined param('del_user')){
        delete_user($del_user);
    }
    # Delete comment
    if (defined param('del_com_num')){
        delete_com();
    }
    # Deletes Post
    if (defined param('del_num')){
        delete_post();
    }
    # Writes comment
    if (defined param('write_com')){
        write_comment();
        $com_num = '';
    }
    # Writes post
    if (defined param('Write')){
        write_post();
        
    }
    # Set current page
    if (defined param('mate')){
        $page_user = $mate;
    }
    # Add mate from their user page
    if (defined param('Add Mate')){
        add_mate($page_user);
    }
    # Checks if logout button is pressed
    if (defined param("Logout")){
        $page_state = '';
        $cookie_user = '';
        $cookie_pass = '';
        $page_user = '';
    }
    # Authorise password
    if (($page_state eq "login_page") || ($page_state eq '')) {
        authorise_password();
    }
    # Print page header
    print page_header();
    if (($page_state eq "login_page") || ($page_state eq '')) {

        if (!($username) && !($password)) {
            login_detail();
            print end_form, "\n";
        } elsif ($username && !($password) && ($page_state eq "login_page")){
            # Show warning for missing password
            login_detail();
            print div({-class=> "matelook_warning"}, "Missing password field!"), "\n";
            print end_form, "\n";
        } elsif (!($username) && $password && ($page_state eq "login_page")) {
            # Show warning for missing username

            login_detail();
            print div({-class=> "matelook_warning"}, "Missing username field!"), "\n";
            print end_form, "\n";

        } elsif ($username && $password && $error == 1) {
            # Show warning for unknown username
            login_detail();
            print div({-class=> "matelook_warning"}, "Unknown Username, Please Type Again"), "\n";
            print end_form, "\n";

        } elsif ($username && $password && $error == 2){
            # Show warnning if password is incorrect
            login_detail();
            print div({-class=> "matelook_warning"}, "Incorrect Password, Please Type Again"), "\n";
            print end_form, "\n";

        }
    } elsif ($page_state eq "user_page") {
        # Change profile image
        if (defined param('submit_profile')){
            my $path = "$users_dir/$cookie_user";
            my $fh = upload('filename');
            my $filename = "profile.jpg";
            my $filename1 = "profile1.jpg";

            my $path2 = "$path/$filename";
            if (-e $path2){
                remove_tree($path2);
            }
            my $path = "$path/$filename1";
            open FILE, '>'.$path;
            while (<$fh>){
                print FILE;
            }
            close FILE;
        }
        if (defined param('submit_background')){
            my $path = "$users_dir/$cookie_user";
            my $fh = upload('filename');
            my $filename = "back.jpg";

            my $path = "$path/$filename";
            if (-e $path){
                remove_tree($path);
            }
            open FILE, '>'.$path;
            while (<$fh>){
                print FILE;
            }
            close FILE;
        }
        if ($mate eq $cookie_user || $page_user eq $cookie_user){
            user_page($cookie_user);
        } elsif (defined param('mate')) {
            my $mate_privacy = check_privacy($mate);
            if ($mate_privacy == 3){
                unknown_page();
            } elsif ($mate_privacy == 2){
                if (is_mate($mate)){
                    user_page($mate);
                } else {
                    unknown_page();
                }
            } else {
                user_page($mate);
            }
        } else {
            my $user_privacy = check_privacy($page_user);
            if ($user_privacy == 3){
                unknown_page();
            } elsif ($user_privacy == 2){
                if (is_mate($page_user)){
                    user_page($page_user);
                } else {
                    unknown_page();
                }
            } else {
                user_page($page_user);
            }
        }

    } elsif ($page_state eq "register_page"){
        register_page();
        
    } elsif ($page_state eq "register_complete"){
        register_complete();
    } elsif ($page_state eq "setting_page"){
        setting_page();
    
    } elsif ($page_state eq "find_password_page"){
        find_password_page();
    }
    print page_trailer();


}
# Checks password 
sub authorise_password{
    
    if ($username && $password){
        $username = substr $username, 0, 256;
        $username =~ s/\W//g;

        my $password_file = "$users_dir/$username/user.txt";
        if (!open F, "<","$password_file") {
            $page_state = "login_page";
            $error = 1;
        } else {
            foreach my $line (<F>){
                chomp $line;
                if ($line =~ /^password=(.*)$/) {
                    $crr_pswd = $1; 
                }
            }
            if ($password eq $crr_pswd){
                $page_state = "user_page";
                $cookie_user = $username;
                $cookie_pass = $password;
                $mate = $username;
                $page_user = $username;
            } else {
                $page_state = "login_page";
                $error = 2;
            }
        }
    } else {
        $page_state = "login_page";
    }
}
# Set forms for login page
sub login_detail{
    print start_form, "\n";
    print div({-class => "matelook_heading"}, "matelook"), "\n";
    print body({-class => "matelook_login"}), "\n";
    print div({-class=> "matelook_login_bar"});
    print div({-class=> "matelook_login_heading"}, "Hello Matey!"), "\n";
    print div({-class=> "matelook_login_text"},"Username:", 
        textfield('username')), "\n";
    print div({-class=> "matelook_login_text"},"Password:", 
        password_field("password")), "\n";
    print submit('Login'), "\n";
    print submit('Register'), "\n";
    print submit('Lost Password'), "\n";

}
# Set forms for lost password
sub find_password_page{
    print start_form, "\n";
    print div({-class => "matelook_heading"}, "matelook"), "\n";
    print body({-class => "matelook_login"}), "\n";
    print div({-class => "matelook_exit"}, submit('Exit')), "\n";
    print div({-class=>"matelook_login_text1"},"Find Password"), "\n";
    print div({-class=>"matelook_login_text1"},"Username:", textfield("reg_user"), submit('Find')), "\n";
    if (!(check_user())){
        if ($reg_user ne ''){
            print div({-class=>"matelook_warning1"}, "Unknown User"), "\n";
        }
        
    } elsif (defined param('Find')){
        print div({-class=>"matelook_warning1"}, "Temporary Password Sent"), "\n";
    
    }
    print end_form,"\n";
    
}
# Set forms for register complete
sub register_complete{
    print start_form, "\n";
    print body({-class => "matelook_login"}), "\n";
    print div({-class => "matelook_heading"}, "matelook"), "\n";
    print div({-class => "matelook_exit"}, submit('Exit')), "\n";
    print div({-class=>"matelook_login_text1"},"Congratulation! You are a mate now!"), "\n";
    print div({-class=>"matelook_login_text2"},"Press back and login!"), "\n";
    print end_form, "\n";
    
}
# Set forms for register page
sub register_page{
    print start_form, "\n";
    print body({-class => "matelook_login"}), "\n";
    print div({-class => "matelook_heading"}, "matelook"), "\n";
    print div({-class => "matelook_exit"}, submit('Exit')), "\n";
    print div({-class=>"matelook_login_text1"},"Register"), "\n";
    print div({-class=>"matelook_login_text2"},"Username must be z followed by 7 digits eg z1234567"), "\n";
    print div({-class=>"matelook_login_text1"},"Username:", textfield("reg_user")), "\n";
    if ($reg_error[0] == 1){
        print div({-class=>"matelook_warning1"}, "write username in correct format"), "\n";
    } elsif ($reg_error[0] == 2){
        print div({-class=>"matelook_warning1"}, "username already taken"), "\n";
        
    }
    print div({-class=>"matelook_login_text1"},"Password:", textfield("reg_pass")), "\n";
    if ($reg_error[1] == 1){
        
        print div({-class=>"matelook_warning1"}, "password field missing"), "\n";
    }
    print div({-class=>"matelook_login_text1"},"Name:", textfield("reg_name")). "\n";
    if ($reg_error[7] == 1){
        print div({-class=>"matelook_warning1"}, "name field missing"), "\n";
    
    }
    print div({-class=>"matelook_login_text1"},"Email:", textfield("reg_email"), submit('Validate')), "\n";
    if ($reg_error[2] == 1){
        print div({-class=>"matelook_warning1"}, "email field missing"), "\n";
    
    }
    if (defined param('Validate')){
        print div({-class=>"matelook_login_text2"},"Type in the number received from email"), "\n";
        print div({-class=>"matelook_login_text1"},"Validate Number:", textfield("reg_num")). "\n";
        
        if ($reg_error[3] == 1){
            print div({-class=>"matelook_warning1"}, "Validation failed"), "\n";

        }

    }
    print div({-class=>"matelook_login_text1"},"Birthday:", textfield("reg_birth")), "\n";
    if ($reg_error[4] == 1){
        print div({-class=>"matelook_warning1"}, "Birthday field missing"), "\n";
    
    }
    print div({-class=>"matelook_login_text1"},"Program:", textfield("reg_pro")), "\n";
    if ($reg_error[5] == 1){
        print div({-class=>"matelook_warning1"}, "Program field missing"), "\n";
    
    }
    print div({-class=>"matelook_login_text1"},"Suburb:", textfield("reg_sbrb")), "\n";
    if ($reg_error[6] == 1){
        print div({-class=>"matelook_warning1"}, "Suburb field missing"), "\n";
    
    }
    print div({-class=>"matelook_login_text2"},"Write course in this format: YEAR Sem CourseCode eg 2016 S1 CHEM1011", "\n");
    print div({-class=>"matelook_login_text2"},"For multiple courses, write comma after each course eg 2016 S1 CHEM1011, 2016 S1 MATH1011"), "\n";
    print div({-class=>"matelook_login_text2"},"Leave blank if there is no course"), "\n";
    print div({-class=>"matelook_login_text1"},"course:", textfield("reg_course")), "\n";
    print div({-class=>"matelook_login_text1"},submit("Sign Up"));
    print end_form, "\n";

    
}
# Check register detail, if correct return 1
# And set warning state
sub check_reg_detail{
    my $out = 1;
    my $count = 0;
    while ($count < 7){
        $reg_error[$count] = 10;
        $count++;
    }
    if (!($reg_user =~ /^z\d{7}$/)){
        $reg_error[0] = 1;
    } elsif (check_user()){
        $reg_error[0] = 2;
    } else {
        $reg_error[0] = 0;
    }
    if ($reg_pass eq ''){
        $reg_error[1] = 1;
    } else {
        $reg_error[1] = 0;
    }
    if ($reg_email eq ''){
        $reg_error[2] = 1;
    } else {
        $reg_error[2] = 0;
    }
    if ($reg_num eq $rand_num){
        $reg_error[3] = 0;
    } else {
        $reg_error[3] = 1;
    }
    if ($reg_birth eq ''){
        $reg_error[4] = 1;
    } else {
        $reg_error[4] = 0;
    }
    if ($reg_pro eq ''){
        $reg_error[5] = 1;
    } else {
        $reg_error[5] = 0;
    }
    if ($reg_sbrb eq ''){
        $reg_error[6] = 1;
    } else {
        $reg_error[6] = 0;
    }
    if ($reg_name eq ''){
        $reg_error[7] = 1;
    } else {
        $reg_error[7] = 0;
    }
    foreach my $err (@reg_error){
        if ($err != 0){
           $out = 0;
           last;
        }
    }
    return $out;

    
}
# Check if username is already registered
sub check_user {
    my @users = glob("$users_dir/*");
    my $out = 0;
    foreach my $u (@users){
        $u =~ s/$users_dir\///;
    }
    foreach my $us (@users){
        if ($reg_user eq $us){
            $out = 1;
        }
    }
    return $out;
}
# Create User
sub create_user{
    my $path = "$users_dir/$reg_user";
    my $file = "user.txt";
    make_path($path);
    $path = "$path/$file";
    open FILE, '>', $path;
    print FILE "zid=$reg_user\n";
    print FILE "password=$reg_pass\n";
    print FILE "email=$reg_email\n";
    print FILE "full_name=$reg_name\n";
    print FILE "birthday=$reg_birth\n";
    print FILE "program=$reg_pro\n";
    print FILE "home_suburb=$reg_sbrb\n";
    print FILE "courses=\[$reg_course\]\n";
    print FILE "mates=\[\]\n";
    close FILE;

}
sub delete_back_img{
    my $path = "$users_dir/$cookie_user/back.jpg";
    if (-e $path){
        unlink $path;
    }
}
sub delete_profile_img {
    my $path = "$users_dir/$cookie_user/profile.jpg";
    my $path1 = "$users_dir/$cookie_user/profile1.jpg";
    if (-e $path){
        unlink $path;
    }
    if (-e $path1){
        unlink $path1;
    }
}
# Write Profile text
sub write_profile_text{
    my $file = "profile.txt";
    my $path = "$users_dir/$cookie_user/$file";
    $profile_text =~ s/\n/\\n/g;
    open FILE, '>'.$path;
    print FILE "text=$profile_text";
    close FILE;
    
}
# Display Profile text
sub display_profile_text{
    my $path = "$users_dir/$page_user/profile.txt";
    my $text = "stop the warnning";
    if (-e $path){
        open TEXT, '<', $path;
        foreach my $line (<TEXT>){
            if ($line =~ /^text=(.*)$/){
                $text = $1;
            }
        }
        $text =~ s/\\n/<br>/g;
        print div({-class=>"matelook_usr_info2"},"$text"),"\n";
    }
}
sub change_detail{
    if (defined param('change_pass') || defined param('change_name') || defined param('change_email')||defined param('change_birth') || defined param('change_pro') || defined param('change_sbrb') ||defined param('change_course')) {
        my @user_detail;
        my $path = "$users_dir/$cookie_user/user.txt";
        open DETAIL, '<', $path;
        foreach my $line (<DETAIL>){
            if (defined param('change_pass') && $reg_pass ne ''){
                if ($line =~ /^password=.*$/){
                    $line = "password=$reg_pass\n";
                    $chng_detail = 1;
                }
            }
            if (defined param('change_name') && $reg_name ne ''){
                if ($line =~ /^full_name=.*$/){
                    $line = "full_name=$reg_name\n";
                    $chng_detail = 1;
                }
            }
            if (defined param('change_email') && $reg_email ne '' && $reg_num eq $rand_num){
                if ($line =~ /^email=.*$/){
                    $line = "email=$reg_email\n";
                    $chng_detail = 1;
                }
            } elsif (defined param('change_email') && $reg_email ne '' && $reg_num ne $rand_num){
                $email_error = 1;
            }
            if (defined param('change_birth') && $reg_birth ne ''){
                if ($line =~ /^birthday=.*$/){
                    $line = "birthday=$reg_birth\n";
                    $chng_detail = 1;
                }
            }
            if (defined param('change_pro') && $reg_pro ne ''){
                if ($line =~ /^program=.*$/){
                    $line = "program=$reg_pro\n";
                    $chng_detail = 1;
                }
            }
            if (defined param('change_sbrb') && $reg_sbrb ne ''){
                if ($line =~ /^home_suburb=.*$/){
                    $line = "home_suburb=$reg_sbrb\n";
                    $chng_detail = 1;
                }
            }
            if (defined param('change_course') && $reg_course ne ''){
                if ($line =~ /^courses=.*$/){
                    $line = "courses=[$reg_course]\n";
                    $chng_detail = 1;
                }
            }
            push @user_detail, $line;
        }
        unlink $path;
        open FILE, '>'.$path;
        foreach my $detail (@user_detail){
            print FILE $detail;
        }
        close FILE;
    }
}
# Sets setting page form
sub setting_page{
    print start_form, "\n";
    print body({-class => "matelook_login"}), "\n";
    print div({-class => "matelook_heading"}, a({-href=>"?mate=$cookie_user"}, "matelook")), "\n";
    print div({-class => "matelook_exit"}, submit('Back')), "\n";
    print div({-class=>"matelook_login_text1"},"Setting"), "\n";
    print div({-class=>"matelook_login_text1"},"Password:", textfield("reg_pass"), 
        submit({-name=>'change_pass', -value=>'Change'})), "\n";
    print div({-class=>"matelook_login_text1"},"Name:", textfield("reg_name"), 
        submit({-name=>'change_name', value=>'Change'})). "\n";
    print div({-class=>"matelook_login_text1"},"Email:", textfield("reg_email"), 
        submit('Validate')), "\n";
    if (defined param('Validate')){
        print div({-class=>"matelook_login_text2"},"Type in the number received from email"), "\n";
        print div({-class=>"matelook_login_text1"},"Validate Number:", textfield("reg_num"), 
            submit({-name=>'change_email', -value=>'Change'})). "\n";
    }
    if ($email_error == 1){
        print div({-class=>"matelook_warning1"}, "Validation failed"), "\n";

    }
    print div({-class=>"matelook_login_text1"},"Birthday:", textfield("reg_birth"), 
        submit({-name=>'change_birth', -value=>'Change'})), "\n";
    print div({-class=>"matelook_login_text1"},"Program:", textfield("reg_pro"), 
        submit({-name=>'change_pro', -value=>'Change'})), "\n";
    print div({-class=>"matelook_login_text1"},"Suburb:", textfield("reg_sbrb"), 
        submit({-name=>'change_sbrb', -value=>'Change'})), "\n";
    print div({-class=>"matelook_login_text2"},"Write course in this format: YEAR Sem CourseCode eg 2016 S1 CHEM1011", "\n");
    print div({-class=>"matelook_login_text2"},"For multiple courses, write comma after each course eg 2016 S1 CHEM1011, 2016 S1 MATH1011"), "\n";
    print div({-class=>"matelook_login_text2"},"Leave blank if there is no course"), "\n";
    print div({-class=>"matelook_login_text1"},"course:", textfield("reg_course"), 
        submit({-name=>'change_course', -value=>'Change'})), "\n";
    if ($chng_detail == 1){
        print div({-class=>"matelook_warning1"}, "Detail Changed, press Back to check"), "\n";
    }
    print end_form, "\n";

}
# Check Privacy
sub check_privacy{
    my $user = "stop warning";
    $user = $_[0];
    my $count = 1;
    my $path = "$users_dir/$user/privacy";
    if (!(-e $path)){
        return 1;
    } else {
        while ($count < 5){
            my $full_path = "$path/$count";
            if (-e $full_path){
                return $count;
            }
            $count++;
        }
    }

}
# Set Privacy
sub set_privacy{
    my $path = "$users_dir/$cookie_user/privacy";
    if (-e $path){
        remove_tree($path);
    } 
    make_path($path);
    $path = "$path/$privacy";
    make_path($path);
    

}
# Set form for unknown page
sub unknown_page{
    my $user_img = "unknown-user.png";
    print start_form, "\n";
    print img({-class=> "matelook_profile_img", src=> "$user_img"}), "\n";
    print div({-class => "matelook_heading"}, a({-href=>"?mate=$cookie_user"}, "matelook")), "\n";
    print end_form, "\n";

}
# Reads user.txt and layout the output for userpage
sub user_page {
    my $curr_page = '';
    $curr_page = $_[0];
    my $curr_privacy = check_privacy($cookie_user); 
    $curr_privacy = "Public" if ($curr_privacy == 1);
    $curr_privacy = "Private" if ($curr_privacy == 2);
    $curr_privacy = "Suspended" if ($curr_privacy == 3);
    my $user_detail = "$users_dir/$curr_page/user.txt";
    my $user_img = "$users_dir/$curr_page/profile.jpg";
    my $back_img = "$users_dir/$curr_page/back.jpg";
    if (!(open P, "<", "$user_img")){
        $user_img = "unknown-user.png";
    }
    my $user_img1 = "$users_dir/$curr_page/profile1.jpg";
    if (-e $user_img1){
        $user_img = $user_img1;
    }
    open U, "<", "$user_detail";
    my $name;
    my $birth;
    my $home_sbrb;
    my $program;
    # Set user variables from user.txt
    foreach my $line (<U>){
        chomp $line;
        if ($line =~ /^full_name=(.*)$/){
            $name = $1;
        } elsif ($line =~ /^birthday=(.*)$/){
            $birth = $1;
        } elsif ($line =~ /^home_suburb=(.*)$/){
            $home_sbrb = $1;
        } elsif ($line =~ /^program=(.*)$/){
    
            $program = $1;
        } elsif ($line =~ /^mates=\[(.*)\]$/){
            @mate = split /,/, $1;
        }
    }
    print start_form, "\n";
    print img({-class=> "matelook_profile_img", src=> "$user_img"}), "\n";
    if (-e $back_img){
        print img({-class=> "matelook_back_img", src=> "$back_img"}), "\n";
    }
    # Put link to users profile on logo
    print div({-class => "matelook_heading"}, a({-href=>"?mate=$cookie_user"}, "matelook")), "\n";
    if (($page_user eq $cookie_user) || (is_mate($page_user))){
        print div({-class => "matelook_usr_name"}, "$name"), "\n";
    
    } else {
        print div({-class => "matelook_usr_name"}, "$name", submit('Add Mate')), "\n";
    }
    print div({-class => "matelook_usr_info"}, "$birth"), "\n",
    div({-class => "matelook_usr_info"}, "Program: $program"), "\n",
    div({-class => "matelook_usr_info"}, "Suburb: $home_sbrb"), "\n";
    if ($cookie_user eq $page_user){
        print div({-class => "matelook_usr_info"}, "Current Privacy Setting: $curr_privacy"), "\n";
    }
    print div({-class=>"matelook_logout"},submit('Logout')), "\n";
    print div({-class=>"matelook_setting"},submit('Setting')), "\n";
    # Display profile text
    if ($cookie_user eq $page_user){
        print div(a({-class=>"matelook_user_p", -href=>"?profile_add=1"}, "Add Profile Text")), "\n";
        if ($profile_add == 1){
            print div({-class=>"matelook_usr_info1"},textarea({-name=>'profile_text', -style=>"resize:none", -row=>"100", -cols=>"43"}), submit({-name=>'write_profile', -value=>'Write'})), "\n";
        }
    }
    if ($profile_add != 1){
        display_profile_text();
    }
    if ($cookie_user eq $page_user){
        print div(a({-class=>"matelook_delete_img", -href=>"?load_image=2"}, "Delete Profile Image")), "\n";
        print div(a({-class=>"matelook_privacy1", -href=>"?privacy=1"}, "Set Privacy to public")), "\n";
        print div(a({-class=>"matelook_privacy2", -href=>"?privacy=2"}, "Set Privacy to private")), "\n";
        print div(a({-class=>"matelook_privacy3", -href=>"?privacy=3"}, "Suspend Account")), "\n";
        print div(a({-class=>"matelook_privacy4", -href=>"?privacy=4"}, "Delete Account")), "\n";
        if ($load_image != 1){
            print div(a({-class=>"matelook_change_img", -href=>"?load_image=1"}, "Change Profile Image")), "\n";
        } elsif ($load_image == 1){
            print div(a({-class=>"matelook_change_img", -href=>"?load_image=1"}, "Change Profile Image")), "\n";
            print div({-class=>"matelook_file"},filefield('filename')), "\n";
            print div({-class=>'matelook_file_submit'}, 
                submit({-name=>'submit_profile', -value=>'Submit'})), "\n";
            
        }
        print div(a({-class=>"matelook_delete_back", -href=>"?load_image=3"}, "Delete Background")), "\n";
        if ($load_image != 4){
            print div(a({-class=>"matelook_change_back", -href=>"?load_image=4"}, "Change Background")), "\n";
        } elsif($load_image == 4){
            print div(a({-class=>"matelook_change_back", -href=>"?load_image=4"}, "Change Background")), "\n";
            print div({-class=>"matelook_file"},filefield('filename')), "\n";
            print div({-class=>'matelook_file_submit'}, 
                submit({-name=>'submit_background', -value=>'Submit'})), "\n";
        
        }
    }
    if ($cookie_user eq $page_user){
        print div({-class => "vertical_line3"}), "\n";
    } else {
        print div({-class => "vertical_line"}), "\n";
    }
    print div({-class => "matelook_title"}, "Mates"), "\n";
    # Creates list of mates
    print "<ul class=\"matelook_mate_list\">", "\n";
    foreach my $member (@mate){
        $member =~ s/\W//g;
        my $mate_img = "$users_dir/$member/profile.jpg";
        my $user_img1 = "$users_dir/$member/profile1.jpg";
        if (-e $user_img1){
            $mate_img = $user_img1;
        }
        my $mate_name = "";
        if (!(open I, "<", "$mate_img")){
            $mate_img = "unknown-user.png";
        }
        $mate_name = mate_name($member);
        if ($cookie_user eq $page_user){
            print "<li class=\"matelook_mate_block\">",
            "<a href=\"?mate=$member\">",
            "<img class=\"matelook_mate_img\" src=\"$mate_img\">$mate_name</a>",
            "<a class=\"matelook_user_del\" href=\"?del_user=$member\">Delete</a></li>","\n";
        } else {
            print "<li class=\"matelook_mate_block\">",
            "<a href=\"?mate=$member\">",
            "<img class=\"matelook_mate_img\" src=\"$mate_img\">$mate_name</a></li>", "\n";
            
        }


    }
    print "</ul>", "\n";
    # Display mate suggestion
    if ($cookie_user eq $page_user){
        print div({-class => "vertical_line2"}), "\n";
        print div({-class => "matelook_title"}, "Mate Suggestion"), "\n";
        mate_suggestion();
    }
    print div({-class => "vertical_line2"}), "\n";
    print div({-class => "matelook_title"}, "Search Mates:", textfield('search'), submit('Search')), "\n";
    # Search mate
    if (defined param('Search')) {
        search_mate();
    }
    print div({-class => "vertical_line2"}), "\n";
    print div({-class => "matelook_title"}, "Search Posts:", textfield('search_p'), submit({-name=>'Search_p', -value=> 'Search'})), "\n";
    # Display Post search results
    if (defined param('Search_p')){
        search_post();
    }

    print div({-class => "vertical_line2"}), "\n";
    print div({-class => "matelook_title"}, "Posts"), "\n";
    print div({-class => "matelook_mini_title"}, "Write Post"), "\n";
    print div(textarea({-name=>'post_text', -style=>"resize:none", -row=>"100", -cols=>"43"}), submit('Write')), "\n";
    # Display user posts
    user_post($curr_page);
    print hidden('State'), "\n",
    hidden('username'), "\n",
    hidden('mate'), "\n",
    end_form, "\n";
}
# Returns name when zID is inputted
sub mate_name {
    my $curr_page = "stop warning";
    $curr_page = $_[0];
    my $name = "";
    my $mate_name = "$users_dir/$curr_page/user.txt";
    open M, "<", "$mate_name";
    foreach my $line (<M>){
        chomp $line;
        if ($line =~ /^full_name=(.*)$/){
            $name = $1;
        }
    }
    return $name;

}
# inputs zid and Returns 1 if mate, else 0
sub is_mate{
    my $user_id = "stop warning";
    my $status = 0;
    $user_id = $_[0];
    my $path = "$users_dir/$cookie_user/user.txt";
    open DETAIL, '<', $path;
    foreach my $line (<DETAIL>){
        if ($line =~ /^mates=\[(.*)\]$/){
            if ($1 =~ /$user_id/){
                $status = 1;
            }
        }
    }
    return $status;

}
# check if a user is mate of mate
# returns multiplier 1 if not
# returns multiplier 1 + 0.2 per each mate of mate
sub is_mate_mate {
    my $mate_id = "stop warning";
    my $count = 1;
    $mate_id = $_[0];
    my $path_user = "$users_dir/$cookie_user/user.txt";
    my $path_mate = "$users_dir/$mate_id/user.txt";
    my @user_mate;
    my @mate_mate;
    open USER, '<', $path_user;
    foreach my $line (<USER>){
        if ($line =~ /^mates=\[(.*)\]$/){
            my $mate_id = $1;
            $mate_id =~ s/\s//g;
            @user_mate = split /,/, $mate_id;
        }
    }
    open MATE, '<', $path_mate;
    foreach my $line (<MATE>){
        if ($line =~ /^mates=\[(.*)\]$/){
            my $mate_id = $1;
            $mate_id =~ s/\s//g;
            @mate_mate = split /,/, $mate_id;
        }
    }
    foreach my $u (@user_mate){
        foreach my $m (@mate_mate){
            if ($u eq $m){
                $count += 0.2;
            }
        }
    }
    #print "@user_mate\n";
    #print "@mate_mate\n";
    return $count;
}
# check if a user does not have same course
# returns 1 if user have same course
# returns 1 + 1.5 per each course
sub same_course{
    my $mate_id = "stop warning";
    my $count = 1;
    $mate_id = $_[0];
    my $path_user = "$users_dir/$cookie_user/user.txt";
    my $path_mate = "$users_dir/$mate_id/user.txt";
    my @user_course;
    my @mate_course;
    open USER, '<', $path_user;
    foreach my $line (<USER>){
        if ($line =~ /^courses=\[(.*)\]$/){
            my $mate_id = $1;
            $mate_id =~ s/\s//g;
            @user_course = split /,/, $mate_id;
        }
    }
    open MATE, '<', $path_mate;
    foreach my $line (<MATE>){
        if ($line =~ /^courses=\[(.*)\]$/){
            my $mate_id = $1;
            $mate_id =~ s/\s//g;
            @mate_course = split /,/, $mate_id;
        }
    }
    foreach my $u (@user_course){
        foreach my $m (@mate_course){
            if ($u eq $m){
                $count += 1.5;
            }
        }
    }
    return $count;
    
}
# Provides html format for mate suggestion
# Maximum of 6 mate suggestion
sub mate_suggestion{
    my @all_user = glob("$users_dir/*");
    my %user_rate;
    my $index = 0;
    foreach my $u (@all_user){
        $u =~ s/$users_dir\///;
        if (!(is_mate($u) || $cookie_user eq $u)){
            $user_rate{$u} = same_course($u) * is_mate_mate($u);  
        }
    }
    print "<ul class=\"matelook_mate_list\">", "\n";
    foreach my $users (reverse(sort { $user_rate{$a} <=> $user_rate{$b}} keys %user_rate)){
        if ($index > 5) {
            last;
        }
        my $mate_img = "$users_dir/$users/profile.jpg";
        my $user_img1 = "$users_dir/$users/profile1.jpg";
        if (-e $user_img1){
            $mate_img = $user_img1;
        }
        if (!(open I, "<", "$mate_img")){
            $mate_img = "unknown-user.png";
        }
        my $name = mate_name($users); 
        print "<li class=\"matelook_mate_block\">",
        "<a href=\"?mate=$users\">",
        "<img class=\"matelook_mate_img\" src=\"$mate_img\">$name</a>",
        "<a class=\"matelook_user_del\" href=\"?add_user=$users\">Add</a></li>","\n";
        $index++;
    }
    print "</ul>", "\n"

    
}
# Add mate
sub add_mate{

    my $user_id = "stop warning";
    $user_id = $_[0];
    my @user_detail;
    my @mate_id;
    my $path = "$users_dir/$cookie_user/user.txt";
    open DETAIL, '<', $path;
    foreach my $line (<DETAIL>){
        if ($line =~ /^mates=\[(.*)\]$/){
            if ($1 eq ''){
                $line =~ s/]/$user_id]/;
            }else{
                $line =~ s/]/, $user_id]/;
            }
        }
        push @user_detail, $line;
    }
    unlink $path;
    open FILE, '>'.$path;
    foreach my $detail (@user_detail){
        print FILE $detail;
    }
    close FILE;
    
}
# Deletes mate
sub delete_user {
    my $user_id = "stop warning";
    $user_id = $_[0];
    my @user_detail;
    my @mate_id;
    my $path = "$users_dir/$cookie_user/user.txt";
    open DETAIL, '<', $path;
    foreach my $line (<DETAIL>){
        if ($line =~ /^mates=\[(.*)\]$/){
            $line =~ s/$user_id,?//;
            $line =~ s/\s//g;
            $line =~ s/,*]$/]/;
            $line = "$line\n";
        }
        push @user_detail, $line;
    }
    unlink $path;
    open FILE, '>'.$path;
    foreach my $detail (@user_detail){
        print FILE $detail;
    }
    close FILE;

    
}
# Returns Profile img or unknown user img if they dont have one
sub mate_img{
    my $curr_user = "stop warning";
    $curr_user = $_[0];
    my $mate_img = "$users_dir/$curr_user/profile.jpg";
    if (!(open I, "<", "$user_img")){
        $mate_img = "unknown-user.png";
    }
    return $mate_img;
}
# Write Post
sub write_post{
    my $count = 0;
    my $file = "post.txt";
    my $post_path = "$users_dir/$page_user/posts/";
    my $post_num = $post_path . $count;
    while (1){
        if (-d $post_num){
            $count++;
            $post_num = $post_path . $count;
            next;
        } else {
            make_path($post_num);
            last;
        }
    }
    $post_num = "$post_num/$file";
    $post_text =~ s/\n/\\n/g;
    open FILE, '>'.$post_num;
    print FILE "from=$cookie_user\n";
    print FILE "message=$post_text\n";
    close FILE;
    
}
# Deletes Post
sub delete_post{
    my $path = "$users_dir/$page_user/posts/$del_num";
    remove_tree($path);
}
# Deletes Comment
sub delete_com{
    my ($post_no, $com_no) = $del_com_num =~ /\((\d*),(\d*)\)/;
    my $path = "$users_dir/$page_user/posts/$post_no/comments/$com_no";
    remove_tree($path);
    
}
# Write comments
sub write_comment {
    my $count = 0;
    my $file = "comment.txt";
    my $comment_dir = "$users_dir/$page_user/posts/$com_num/comments";
    my $comment_num = "$comment_dir/$count";
    if (!(-d $comment_dir)){
        make_path($comment_dir);    
    }
    while(1){
        if (-d $comment_num){
            $count++;
            $comment_num = "$comment_dir/$count";
            next;
        } else {
            make_path($comment_num);
            last;
        }
    }
    $com_text =~ s/\n/\\n/g;
    $comment_num = "$comment_num/$file";
    open FILE, '>'.$comment_num;
    print FILE "from=$cookie_user\n";
    print FILE "message=$com_text\n";
    close FILE;
        
}
# Searches for post with regex
sub search_post{
    my $msg;
    my $from;
    my @users = glob("$users_dir/*");
    foreach my $user (@users){
        $user =~ s/$users_dir\///g;
    }
    print "<div class=\"matelook_search_post\">", "\n";
    foreach my $curr_user (@users){
        my $path = "$users_dir/$curr_user/posts";
        my @posts = glob("$users_dir/$curr_user/posts/*");
        foreach my $post_num (@posts) {
            $post_num =~ s/$users_dir\/$curr_user\/posts\///g;
        }
        @posts = reverse(sort {$a <=> $b} @posts);
        foreach my $post_d (@posts){
            open P, "<", "$path/$post_d/post.txt";
            foreach my $line (<P>){
                chomp $line;
                if ($line =~ /^message=(.*)$/){
                    $msg = $1;
                    $msg =~ s/\\n/<br>/g;
                    $msg =~ s/(z\d{7})/add_anchor($1)/eg; 
                }
                if ($line =~ /^from=(.*)$/){
                    $from = $1;
                    $from = add_anchor($from);
                }
            }
            if ($msg =~ /$search_p/){
                print div({-class=>"matelook_post"},"$from <br>$msg"), "\n";
            }
        }
    }
    print "</div>", "\n";

}
# Prints user's posts
sub user_post{
    my $curr_user = "stop the warning";
    my $msg;
    my $from;
    my $zid;
    $curr_user = $_[0];
    my $path = "$users_dir/$curr_user/posts";
    my @posts = glob("$users_dir/$curr_user/posts/*");
    foreach my $post_num (@posts) {
        $post_num =~ s/$users_dir\/$curr_user\/posts\///g;
    }
    @posts = reverse(sort {$a <=> $b} @posts);

    foreach my $post_d (@posts){
        open P, "<", "$path/$post_d/post.txt";
        my $com_path = "$path/$post_d/comments";
        my @comment = glob("$path/$post_d/comments/*");
        foreach my $com_num (@comment) {
            $com_num =~ s/$com_path\///g;
        }
        @comment = reverse(sort {$a <=> $b} @comment);

        foreach my $line (<P>){
            chomp $line;
            if ($line =~ /^message=(.*)$/){
                $msg = $1;
                $msg =~ s/\\n/<br>/g;
                $msg =~ s/(z\d{7})/add_anchor($1)/eg; 
            }
            if ($line =~ /^from=(.*)$/){
                $from = $1;
                $zid = $from;
                $from = add_anchor($from);
            }
        }
        print div({-class=>"matelook_post"},"$from <br>$msg"), "\n";
        # Add delete or comment link depending on the owner of posts
        if ($zid eq $cookie_user){
            print a({-href=>"?del_num=$post_d", -class=>"matelook_post_a"},"delete"), "\n";
            print a({-href=>"?com_num=$post_d", -class=>"matelook_comment_a"},"comment"), "\n";
            if ($post_d eq $com_num) {
                print div(textarea({-name=>'com_text', -style=>"resize:none", -row=>"100", -cols=>"43"}), submit({-name=>"write_com", -value=>"Write"})), "\n";
                print hidden('com_num'), "\n";
            }
        } else {
            print a({-href=>"?com_num=$post_d", -class=>"matelook_post_a"},"comment"), "\n";
            if ($post_d eq $com_num) {
                print div(textarea({-name=>'com_text', -style=>"resize:none", -row=>"100", -cols=>"43"}), submit({-name=>"write_com", -value=>"Write"})), "\n";
                print hidden('com_num'), "\n";
            }
        }
        foreach my $comment_d (@comment){
            open C, "<", "$com_path/$comment_d/comment.txt";
            foreach my $line (<C>){
                if ($line =~ /^message=(.*)$/){
                    $msg = $1;
                    $msg =~ s/\\n/<br>/g;
                    $msg =~ s/(z\d{7})/add_anchor($1)/eg; 
                }
                if ($line =~ /^from=(.*)$/){
                    $from = $1;
                    $zid = $from;
                    $from = add_anchor($from);
                }

            }
            print div({-class=>"matelook_comment"},"From $from, <br>$msg"), "\n";
            if ($zid eq $cookie_user){
                print div({-class=>"matelook_post_a2"},a({-href=>"?del_com_num=($post_d,$comment_d)"},"delete")), "\n";
            }
        }
    }

}
# Add anchor to zids
sub add_anchor {
    my $zid = "";
    $zid = $_[0];
    my $name = mate_name($zid);
    return "<a href=\"?mate=$zid\">$name</a>",
}
# Searches user give regex
sub search_mate {
    my @users = sort(glob("$users_dir/*"));
    my $name = "stop warning";
    my $zid = "stop warning";
    print "<ul class=\"matelook_mate_list\">", "\n";
    foreach my $user (@users){
        open U, "<", "$user/user.txt";
        $user =~ s/$users_dir\///;
        my $mate_img = "$users_dir/$user/profile.jpg";
        if (!(open I, "<", "$mate_img")){
            $mate_img = "unknown-user.png";
        }
        foreach my $line (<U>) {
            chomp $line;
            if ($line =~ /^full_name=(.*)$/){
                $name = $1;
            } elsif ($line =~ /^zid=(.*)$/){
                $zid = $1;
            } 

        }
        if ($name =~ /$search/i){
            if (is_mate($zid) || ($zid eq $cookie_user)){
                print "<li class=\"matelook_mate_block\">",
                "<a href=\"?mate=$user\">",
                "<img class=\"matelook_mate_img\" src=\"$mate_img\">$name</a></li>", "\n";
            } else {
                print "<li class=\"matelook_mate_block\">",
                "<a href=\"?mate=$user\">",
                "<img class=\"matelook_mate_img\" src=\"$mate_img\">$name</a>",
                "<a class=\"matelook_user_del\" href=\"?add_user=$zid\">Add</a></li>","\n";
                
            }
        }

    }
    print "</ul>", "\n"
}
#
# HTML placed at the top of every page
#
sub page_header {
    return header(-charset => "utf-8", -cookie=>["state = $page_state", "cookie_user = $cookie_user", "cookie_pass = $cookie_pass", "page_user = $page_user", "rand_num = $rand_num"]),
    start_html(-title => 'matelook', -style => "matelook.css"),
}


#
# HTML placed at the bottom of every page
# It includes all supplied parameter values as a HTML comment
# if global variable $debug is set
#
sub page_trailer {
    my $html = "";
    $html .= join("", map("<!-- $_=".param($_)." -->\n", param())) if $debug;
    $html .= end_html;
    return $html;
}

main();
